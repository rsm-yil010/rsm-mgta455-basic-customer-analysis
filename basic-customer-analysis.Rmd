---
title: "The Bookbinders Book Club: Basic Customer Analysis"
author: "Professor Vincent Nijs - Rady School of Management (UCSD)"
output:
  html_document:
    highlight: zenburn
    theme: cosmo
    df_print: paged
    toc: yes
    code_folding: show
    code_download: true
---

```{r r_setup, include = FALSE}
## initial settings
knitr::opts_chunk$set(
  comment = NA,
  echo = TRUE,
  error = TRUE,
  cache = FALSE,
  message = FALSE,
  dpi = 144,
  warning = FALSE
)

## width to use when printing tables etc.
options(
  width = 250,
  scipen = 100,
  max.print = 5000,
  stringsAsFactors = FALSE
)

## make all required libraries available by loading radiant package if needed
library(radiant)

## load bbb dataset
bbb <- readr::read_rds("data/bbb.rds")  
```

<style>
.btn, .form-control, pre, code, pre code {
  border-radius: 4px;
}
.table {
  width: auto;
}
ul, ol {
  padding-left: 18px;
}
code, pre, pre code {
  overflow: auto;
  white-space: pre;
  word-wrap: normal;
}
code {
  color: #c7254e;
  background-color: #f9f2f4;
}
pre {
  background-color: #ffffff;
}
</style>

## Question 1: What percentage of BookBinder's customers are female?

![](figures/Q1.png)

```{r}
result <- pivotr(
  bbb, 
  cvars = "gender", 
  normalize = "total", 
  nr = 2
)
summary(result, perc = TRUE, dec = 2)
```

66.6% of BookBinder's customers are female (_Data > Pivot_)

Using dplyr:

```{r}

```

## Question 2: Which three states have the most customers?

![](figures/Q2.png)

```{r}
result <- pivotr(
  bbb, 
  cvars = "state", 
  tabsort = "desc(n_obs)", 
  nr = 3
)
summary(result)
dtab(result) %>% render()
```

The three states with the most customers are NY, NJ, and PA. Note the use of `tabsort = "desc(n_obs)"` to sort the table and `nr = 3` to show the first three rows in the command above. In the browser interface you can sort the table by clicking on the column header (_Data > Pivot_)

Using dplyr:

```{r}

```

## Question 3: What are the mean and standard deviation for `last` (months since last purchase), `total` (\$ spent), and `purch` (number of books purchased)?

![](figures/Q3.png)

```{r}
result <- explore(
  bbb, 
  vars = c("last", "total", "purch"), 
  fun = c("mean", "sd")
)
summary(result, dec = 2)
```

Using _Data > Explore_ the mean and standard deviation are shown in the table above

Using dplyr:

```{r}

```

## Question 4: What is the correlation between customers' total spending on books (`book`) and total spending on non-book products (`nonbook`)?

![](figures/Q4.png)

```{r fig.width=5.92, fig.height=5.92, dpi = 144}
result <- correlation(bbb, vars = c("book", "nonbook"))
summary(result)
plot(result, nrobs = 1000)
```

The number of `book` and `nonbook` purchases are positively correlated (0.16). The `p.value` for the correlation is less than the usual _signficance level_ of 0.05 so we can conclude the correlation is _statistically signficant_. See _Basics > Correlation_.

Using base R:

```{r}
cor.test(bbb$book, bbb$nonbook)
```

## Question 5: In which category does BBB sell most books? In which category does BBB sell the least number of books?

![](figures/Q5.png)

```{r}
result <- explore(
  bbb, 
  vars = c(
    "child", "youth", "cook", "do_it", 
    "reference", "art", "geog"
  ), 
  fun = "sum", 
  tabsort = "desc(sum)"
)
summary(result)
```

BBB sells most books in the cooking category (`cook`). The company sells the fewest books in the `reference` category. Note, again, the use of `tabsort = "desc(sum)"` to sort the table. In the browser interface (_Data > Explore_) sort the data by clicking on the column header

Using dplyr:

```{r}

```

## Question 6: Create a bar chart of the average total spending on books for males and females

![](figures/Q6-pivot.png)

```{r fig.width=8.08, fig.height=4.31, dpi = 144}
result <- pivotr(
  bbb, 
  cvars = "gender", 
  nvar = "book"
)
summary(result, dec = 1)
plot(result)
```

The plot can be created in either _Data > Pivot_ or _Data > Visualize_.

![](figures/Q6-viz.png)

Using dplyr and ggplot2:

```{r}

```

## Question 7: How many male and female customers bought The Art History of Florence? What percentage of male customers bought the book? What percentage of female customers bought the book?

To create a table with frequencies use _Data > Pivot_.

![](figures/Q7-nr.png)

```{r}
result <- pivotr(bbb, cvars = c("gender", "buyer"))
summary(result)
```

To get the percentage of male and female buyers normalize by `column`.

![](figures/Q7-perc.png)

```{r}
result <- pivotr(
  bbb, 
  cvars = c("gender", "buyer"), 
  normalize = "column"
)
summary(result, perc = TRUE, dec = 2)
```

2,389 female and 2,133 male customers bought _The Art History of Florence_. The percentage of female customers that bought the book was 7.17%. The percentage of male customers that bought the book was 12.77%.

We could also use _Data > Explore_ to create a table with summary statistics. For `factors` Radiant will use the occurence of the first level of the variable in calculations (i.e., 'yes' for buyer).

Using dplyr:

```{r}

```

## Question 8: Determine the total and average number of purchases for both male and female customers

![](figures/Q8.png)

```{r}
result <- explore(
  bbb, 
  vars = "purch", 
  byvar = "gender", 
  fun = c("sum", "mean")
)
summary(result)
```

Using _Data > Explore_ we see that female customers purchased 111,968 books from BBB (3.362 per female customer on averge). Male customers purchased 82,543 books from BBB (4.943 per male customer on averge).

Using dplyr:

```{r}

```

## Question 9: Determine the minimum, maximum, average number of months between customers' first and most recent purchase.

![](figures/Q9.png)

We first need to create a new variable (e.g., `mdiff`) that captures the difference between `first` and `last` using _Data > Transform > Create_.

    mdiff = first - last

```{r}
## create the month difference variable
bbb <- mutate(bbb, mdiff = first - last)
```

Then we can use _Data > Explore_ to calculate the desired summary statistics.

```{r}
result <- explore(
  bbb, 
  vars = "mdiff", 
  fun = c("min", "max", "mean")
)
summary(result)
```

The minimum time difference is 0 (i.e., the last purchase was also the first). The maxiumum difference was 72 months and the average difference was 13.311 months.

Using dplyr:

```{r}

```

## Question 10: What percentage of repeat customers (i.e., those with two or more total purchases) bought The Art History of Florence?

![](figures/Q10.png)

Here we need to apply a `filter` to select only those customers that have two or more purchase:

    purch >= 2

Then we can use _Data > Pivot_ to get the desired numbers.

```{r}
result <- pivotr(
  bbb, 
  cvars = "buyer", 
  normalize = "total", 
  data_filter = "purch >= 2"
)
summary(result, perc = TRUE, dec = 2)
```

10.32% of repeat purchasers bought _The Art History of Florence_.

Using dplyr:

```{r}

```

